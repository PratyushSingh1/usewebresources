package com.useWebResources.controllers;

import java.io.File;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.representation.Form;

/**
 * Servlet implementation class HitURLServlet
 */
public class HitURLServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HitURLServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String userName=request.getParameter("userName");
		String userEmail=request.getParameter("userEmail");
		Client client=new Client();
		Form form=new Form();
		form.add("userName",userName);
		form.add("userEmail",userEmail);
		/*
		 * form param hitting
		 * WebResource resource=client.resource(
		 * "http://localhost:9898/restfulWebProducer/backend/user/service/update/pratyush?number=9869614041"
		 * );
		 */
		
		//extract all parameters using uriInfo method.
		/*
		 * WebResource resource=client.resource(
		 * "http://localhost:9898/restfulWebProducer/backend/user/service/getURIParams/pathParam1/pathparam2?param1=pratyush&param2=Singh"
		 * );
		 */
		
		//get header parameter
		/*
		 * WebResource resource=client.resource(
		 * "http://localhost:9898/restfulWebProducer/backend/user/service/getHeaders");
		 */
		
		//submit form param and get it as whole one
		/*
		 * WebResource resource=client.resource(
		 * "http://localhost:9898/restfulWebProducer/backend/user/service/submitFormParam"
		 * );
		 */
		
		//get response back as plain text 
		
		  WebResource resource=client.resource(
		  "http://localhost:9898/restfulWebProducer/backend/user/service/getResponseAsXML?name="
		  +userName+"&email="+userEmail);
		 
	
		//here the method name can change as required from get to post....
		/*
		 * ClientResponse
		 * clientResponse=resource.accept("test/html").post(ClientResponse.class,form);
		 */
		
		/*
		 * WebResource resource=client.resource(
		 * "http://localhost:9898/restfulWebProducer/backend/user/service/processInput")
		 * ;
		 */
		
		
		  ClientResponse
		  clientResponse=resource.accept("application/json").get(ClientResponse.class);
		 
		
		/*
		 * ClientResponse clientResponse=resource.accept("text/plain")
		 * .entity(userName,"text/plain") .post(ClientResponse.class);
		 */
		
		if(clientResponse.getStatus()==200) {
			String output=clientResponse.getEntity(String.class);
			System.out.println(output);
		}else {
			System.out.println("ERROR IN RESPONSE");
		}
		
	}

}
